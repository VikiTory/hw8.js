/*---Теоретичні питання---
1) Опишіть своїми словами що таке Document Object Model (DOM)
DOM - це деревоподібна структура, яка представляє HTML. Це дозволяє JS
отримувати доступ і керувати елементам, атрибутами та вмістом документа.
2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML - повертає або встановлює вміст HTML, включаючи теги HTML елемента.
innerText - повертає або встановлює текстовий вміст елемента з вилученими тегами
(повертає або встановлює текстовий вміст, який буде видимий для користувача). 
3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
Є такі способи посилання на елемент сторінки за допомогою методів JS:
getElementById(); getElementByClassName(); getElementByNagName(); querySelector(); 
querySelectorAll(). Взагалі немає кращого методу для посилання на елемент сторінки,
адже це залежить від конкретного випадку використання та структури документа. Однак 
метод getElementById() зазвичай є найефективнішим і часто використовується для націлювання
на окремі елементи.

---Практичне завдання---*/

const p = document.getElementsByTagName("p");
const optionsList = document.getElementById("optionsList");
const parentElement = optionsList.parentElement;
const childNodes = parentElement.childNodes;
const testParagraph = document.getElementById('testParagraph');
testParagraph.innerHTML = '<p>This is the next paragraph.</p>';
const mainHeader = document.querySelectorAll('.main-header');
const navItems = document.querySelectorAll('.nav-list');
const sectionTitels = document.querySelectorAll('.section-title');

sectionTitels.forEach(function(title) {
    title.classList.remove('section-title');
});

navItems.forEach((navItem) => {
    navItems.classList.add('nav-item');
});

for (i = 0; i < childNodes.length; i++) {
    console.log("Node Name:" + childNodes[i].nodeName + "Node Type:" + childNodes[i].nodeType);
}

for (let i = 0; i < p.length; i++) {
    p[i].style.backgroundColor = "#ff0000";
}



console.log(optionsList);
console.log(parentElement);
console.log(mainHeader);
console.log(navItems);







